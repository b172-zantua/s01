= Mini activity link
https://docs.google.com/document/d/1sTrjFBdgWlbybwxdOwSNpAb6iGi8-k0NUqWKXU3JCbQ/edit?usp=sharing


= Activity Solution
1. - The Busy Executive's Database Guide
   - You Can Combat Computer Stress!

2. - Cooking with Computers
   - (TC7777)

3. - Marjorie Green
   - Abraham Bennet

4. - Algodata Infosystems

5. - The Busy Executive's Database Guide
   - Cooking with Computers
   - Straight Talk About Computers
   - But Is It User Friendly?
   - Secrets of Silicon Valley
   - Net Etiquette